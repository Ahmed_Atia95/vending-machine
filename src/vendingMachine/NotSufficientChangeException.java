package vendingMachine;

public class NotSufficientChangeException extends RuntimeException
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String msg;

    public NotSufficientChangeException(String string)

    {
        this.msg = string;
    }

    @Override public String getMessage()

    {
        return msg;
    }
}
