package vendingMachine;

public class Bucket<M, N> {
    private M first;
    private N second;

    public Bucket(M first, N second)
    {
        this.first = first;
        this.second = second;
    }

    public M getFirst()

    {
        return first;
    }

    public N getSecond()

    {
        return second;
    }
}
