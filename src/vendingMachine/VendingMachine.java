package vendingMachine;

import java.util.List;


public interface VendingMachine {
    public long selectItemAndGetPrice(Item item);
    public void insertCoin(Coin coin);
    public List<Coin> refund();
    public Bucket<Item, List<Coin>> collectItemAndChange() throws SoldOutException, NotSufficientChangeException, NotFullPaidException;
    public void reset();
}
