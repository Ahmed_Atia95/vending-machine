package vendingMachine;

public class NotFullPaidException extends Exception {

    private String msg;
    private long remaining;

    public NotFullPaidException(String msg, long remaining) {

        this.msg = msg;
        this.remaining = remaining;
    }

    public long getRemaining()

    {
        return remaining;
    }


    @Override public String getMessage()
    {
        return msg + remaining;
    }
}